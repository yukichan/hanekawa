package main

import (
  "flag"
  "log"
  "gitlab.com/yukichan/hanekawa/config"
  "gitlab.com/yukichan/hanekawa/server"
)

func main() {
  conf := loadConf()
  server.InitServer(conf)
  server.RunServer()
}

func loadConf() config.Config {
  var confLoc = flag.String("conf", "/etc/hanekawa/conf.toml", "The path to the config file to load")
  var runMode = flag.String("mode", "default", "The mode, specified in the config file, which the server should run in")
  flag.Parse()
  conf, err := config.LoadConfig(*confLoc, *runMode)
  if err != nil {
    log.Fatalf("Terminating due to fatal error whilst loading configuration...")
  }
  return conf
}
