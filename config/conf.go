package config

import (
  "github.com/BurntSushi/toml"
  "io/ioutil"
  "log"
  "errors"
)

var emptyConf Config
type Config struct {
  //Saved command line arguments
  ConfLoc string
  RunMode string
  //MongoDB connection settings
  MongoHost string      `toml:"MongoAddress"`
  MongoPort int         `toml:"MongoPort"`
  MongoDatabase string  `toml:"MongoDatabase"`
  //Web server settings
  UseTLS bool           `toml:"UseHTTPS"`
  HTTPPort int          `toml:"BindPort"`
  HTTPAddr string       `toml:"BindAddr"`
  ReleaseMode bool      `toml:"ProductionMode"`
  //General options
  CollectionLimit int   `toml:"MaxCategories"`
  TriggerLimit int      `toml:"MaxTriggersPerCategory"`
}


func LoadConfig(path string, mode string) (Config, error) {
  conf := make(map[string]Config)
  file, err := ioutil.ReadFile(path)
  if err != nil {
    log.Printf("Could not open file %s to read, failed with error %q", err)
    return emptyConf, err
  }

  rawToml := string(file)
  _, err = toml.Decode(rawToml, &conf)
  if err != nil {
    log.Printf("Could not parse config file %s, failed with error %q", path, err)
    return emptyConf, err
  }

  if val, ok := conf[mode]; ok {
    val.ConfLoc = path
    val.RunMode = mode
    return val, nil
  } else {
    log.Printf("Could not find settings for runmode %s in config file %s", mode, path)
    return emptyConf, errors.New("config file did not contain the requested runmode")
  }
}
