package server

import (
  "gopkg.in/gin-gonic/gin.v1"
  "gopkg.in/mgo.v2"
  "fmt"
  "time"
  "log"
  "gitlab.com/yukichan/hanekawa/store"
  "gitlab.com/yukichan/hanekawa/models"
  "gitlab.com/yukichan/hanekawa/config"
  "net/http"
)

func AddApiEndpoints(srv Server) {
  r := srv.Router

  r.GET("/api/categories", GetCatsHandler)
  //r.GET("/api/triggers", GetTrigsHandler)

  r.POST("/api/categories", PostCatsHandler)
  //r.POST("/api/triggers", PostTrigsHandler)
  r.POST("/api/triggers/xml", PostXMLTrigsHandler)
  //r.PUT("/api/triggers", ModifyTrigsHandler)
}

func GetCatsHandler(c *gin.Context) {
  db := c.MustGet("store").(*mgo.Database)
  conf := c.MustGet("conf").(config.Config)

  cats, err := store.GetAllCategories(db, conf)
  if err != nil {
    c.String(500, fmt.Sprintf("%s", err))
  } else {
    c.JSON(http.StatusOK, cats)
  }
}

func PostCatsHandler(c *gin.Context) {
  db := c.MustGet("store").(*mgo.Database)
  conf := c.MustGet("conf").(config.Config)

  var newCat models.Category
  c.BindJSON(&newCat)
  err := store.AddCategory(db, conf, newCat)
  if err != nil {
    c.String(500, fmt.Sprintf("%s", err))
  } else {
    c.Status(http.StatusCreated)
  }
}

func GetTrigsHandler(c *gin.Context) {
  db := c.MustGet("store").(*mgo.Database)
  conf := c.MustGet("conf").(config.Config)

  cats, err := store.GetAllCategories(db, conf)
  if err != nil {
    c.String(500, fmt.Sprintf("%s", err))
  } else {
    c.JSON(http.StatusOK, cats)
  }
}

func PostXMLTrigsHandler(c *gin.Context) {
  db := c.MustGet("store").(*mgo.Database)
  conf := c.MustGet("conf").(config.Config)

  query := struct{
    Category string  `json:"category"`
    XMLString string `json:"triggers"`
  }{}
  c.BindJSON(&query)
    
  triggers, err := models.FromXML(query.XMLString)
  if err != nil {
    log.Printf("XML parse failed, now aborting.")
    c.JSON(400, map[string]string{
      "errorCode": "101",
      "errorMsg":  "XML parse failed, please check syntax.",
    })
    return
  }

  foundCat, err := store.GetCategory(db, conf, query.Category)
  var catId int
  if err != nil {
    log.Printf("Category not found, aborting...")
    c.JSON(400, map[string]string{
      "errorCode": "102",
      "errorMsg":  "Invalid category given, abortin data entry.",
    })
    return
  } else {
    catId = foundCat.Id
  }
  for _, trigger := range triggers {
    trigger.Status = 0
    trigger.Rating = 1
    trigger.Uploaded = time.Now()
    trigger.Category = catId
    store.AddTrigger(db, conf, trigger)
  }
  c.Status(http.StatusCreated)
}
