package server 

import (
  "gopkg.in/gin-gonic/gin.v1"
)

func (srv Server) InitMiddlewares() {
  srv.Router.Use(AttachDB)
  srv.Router.Use(AttachConfig)
}


func AttachDB(c *gin.Context) {
  session := GlobalServerObject.Store.Session.Clone()

  defer session.Close()

  dbName := GlobalServerObject.Config.MongoDatabase
  c.Set("store", session.DB(dbName))
  c.Next()
}

func AttachConfig(c *gin.Context) {
  conf := GlobalServerObject.Config
  c.Set("conf", conf)
  c.Next()
}
