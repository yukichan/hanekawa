package server

import (
  "gopkg.in/gin-gonic/gin.v1"
  "net/http"
  "log"
  "fmt"
  "gitlab.com/yukichan/hanekawa/config"
  "gitlab.com/yukichan/hanekawa/store"
)

//A Globally-accessible server object to use from within requests
var GlobalServerObject Server

type Server struct {
  //Server configs
  Config config.Config

  //Data Store connection
  Store store.DataStore

  //Web server engine
  Router *gin.Engine
}


func InitServer(config config.Config) {
  //Add config object
  GlobalServerObject.Config = config

  //Connect to Mongo
  storeStruct, err := store.EstablishConnection(config.MongoHost, config.MongoPort)
  if err != nil {
    log.Fatalf("Server Initialization failed, now terminating...")
  }
  GlobalServerObject.Store = storeStruct

  //Create gin instance
  router := gin.Default()
  GlobalServerObject.Router = router
  if config.ReleaseMode {
    gin.SetMode(gin.ReleaseMode)
  }

  //Call other initialisation functions
  GlobalServerObject.InitMiddlewares()
  GlobalServerObject.InitRoutes()
}


func (srv Server) Run() {
  if !srv.Config.UseTLS {
    addr := fmt.Sprintf("%s:%d", srv.Config.HTTPAddr, srv.Config.HTTPPort)
    srv.Router.Run(addr)
  } else {
    log.Fatalf("TLS support in progress...")
  }

}

func RunServer() {
  GlobalServerObject.Run()
}


func (srv Server) InitRoutes() {
  AddApiEndpoints(srv)
  srv.Router.GET("/", func(c *gin.Context) {
    c.String(http.StatusOK, "Hello")
  })
}
