package server

import (
  "bytes"
  "fmt"
  "gopkg.in/gin-gonic/gin.v1"
  "gopkg.in/mgo.v2/bson"
  "gopkg.in/mgo.v2"
  "gitlab.com/yukichan/hanekawa/models"
  "gitlab.com/yukichan/hanekawa/config"
  "gitlab.com/yukichan/hanekawa/store"
  "strconv"
  "encoding/base64"
  "encoding/binary"
  "log"
  "strings"
)

/* Query Format:
** ?cats=
** &ver=<api_version>
** ======== OPTIONAL BELOW HERE ===========
** &cats=<category_base64>
** &stat=<minimum_status>
** &rat=<minimum_rating>
** &excat=[categories_excluded]

/* Category Format (Base64):
** |------------------------------------|
** |   Bits  |  Content                 |
** |---------|--------------------------|
** | 0-7     | Included Category Types  |
** | 16-47   | Selected Tiers           |
** | 48-55   | Major Patch No.          |
** |____________________________________|
*/

type CatQuery1 struct {
  Types byte
  Tiers uint32
  Expansion byte
}


func DecodeCatQuery(base64q string) (CatQuery1, error) {
  var settings CatQuery1
  data, err := base64.URLEncoding.DecodeString(base64q)
  if err != nil {
    log.Printf("Could not decode category filter string %s due to error %s", base64q, err)
    return settings, err
  }
  buf := bytes.NewReader(data)
  err = binary.Read(buf, binary.LittleEndian, &settings)
  if err != nil {
    log.Printf("Category filter was invalid; returned error %s", err)
    return settings, err
  }
  return settings, nil
}

func RunQuery(c *gin.Context) ([]models.Trigger, error) {
  version := c.Query("ver")
  switch version {
  case "1":
    return queryVer1(c)
  default:
    return nil, fmt.Errorf("Version number %s was not recognised.", version)
  }
}

func queryVer1(c *gin.Context) ([]models.Trigger, error) {
  db := c.MustGet("store").(*mgo.Database)
  conf := c.MustGet("conf").(config.Config)
  var catsListFilters []interface{}
  var trigsListFilters []interface{}
  catQuery, err := DecodeCatQuery(c.Query("cats"))
  minStatus := c.Query("stat")
  minRating := c.Query("rat")
  excludedCats := c.Query("excat")

  //Trigger Status Filters
  if minStatus != "" {
    minStatNo, err := strconv.Atoi(minStatus)
    if err == nil {
      trigsListFilters = append(trigsListFilters, bson.M{"status": bson.M{"$gte": minStatNo}})
    }
  }

  //Trigger Rating Filters
  if minRating  != "" {
    minRatNo, err := strconv.Atoi(minRating)
    if err == nil {
      trigsListFilters = append(trigsListFilters, bson.M{"rating": bson.M{"$gte": minRatNo}})
    }
  }

  //Main Category Filters
  if c.Query("cats") != "" {
    if err == nil {
      log.Printf("Aborting Query.")
      return nil, err
    }
    var typesFilter []interface{}
    var tiersFilter []interface{}
    var patchFilter []interface{}
    for i:=uint(1); i<=3; i++ {
      if catQuery.Types << i != 0 {
        typesFilter = append(typesFilter, bson.M{"type": i})
      }
    }
    for i, _ := range models.GetTiers() {
      if catQuery.Tiers << uint(i) != 0 {
        tiersFilter = append(tiersFilter, bson.M{"tier": i})
      }
    }
    for i:=uint(1); i<=3; i++ {
      if catQuery.Expansion << i != 0 {
        patchFilter = append(patchFilter, bson.M{"expansion": i})
      }
    }

    if len(typesFilter) > 0 {
      catsListFilters = append(catsListFilters, bson.M{"$or": typesFilter})
    }
    if len(tiersFilter) > 0 {
      catsListFilters = append(catsListFilters, bson.M{"$or": tiersFilter})
    }
    if len(patchFilter) > 0 {
      catsListFilters = append(catsListFilters, bson.M{"$or": patchFilter})
    }
  }

  //Explicitly excluded categories
  if excludedCats != "" {
    catsList := strings.Split(excludedCats, ",")
    for _, cat := range catsList {
      catNo, err := strconv.Atoi(cat)
      if err == nil{
        catsListFilters = append(catsListFilters, bson.M{"_id": bson.M{"$ne": catNo}})
      }
    }
  }
  catsQuery := bson.M{"$and": catsListFilters}
  cats, err := store.GetCategories(db, conf, catsQuery)
  if err != nil {
    log.Printf("Could not retrieve data from database due to error %s", err)
    return nil, err
  }
  var triggers []models.Trigger
  for _, category := range cats {
    trigs, err := store.GetTriggersForCategory(db, conf, category, trigsListFilters)
    if err != nil {
      log.Printf("Could not fetch triggers from database due to error %s", err)
      return nil, err
    } else {
      triggers = append(triggers, trigs...)
    }
  }
  return triggers, nil

}
