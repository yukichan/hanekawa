package store

import (
  "gopkg.in/mgo.v2"
  "fmt"
  "log"
)

type DataStore struct {
  Session *mgo.Session
  Mongo *mgo.DialInfo
}

func EstablishConnection(addr string, port int) (DataStore, error) {
  var res DataStore
  uri := fmt.Sprintf("%s:%d", addr, port)
  mongo, err := mgo.ParseURL(uri)
  res.Mongo = mongo

  session, err := mgo.Dial(uri)
  if err != nil {
    log.Printf("Could not connect to mongo server at %s: failed with error %s", uri, err)
    return res, err
  }
  res.Session = session

  session.SetSafe(&mgo.Safe{})
  log.Printf("Connected to mongo server at %s.", uri)
  return res, nil
}
