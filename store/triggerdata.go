package store

import (
  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"
  "gitlab.com/yukichan/hanekawa/models"
  "gitlab.com/yukichan/hanekawa/config"
  "errors"
  "log"
  "fmt"
  "strconv"
)

func GetNextIndex(db *mgo.Database, index string) (int, error) {
  indicesColl := db.C("indices")
  num, err := indicesColl.FindId(index).Count()
  if err != nil {
    log.Printf("Could not check indices due to error %s", err)
    return 0, err
  }

  if num > 0 {
    var res bson.M
    mod := mgo.Change {
      Update:     bson.M{"$inc": bson.M{"idx": 1}},
      ReturnNew:  true,
    }
    _, err := indicesColl.FindId(index).Apply(mod, &res)
    if err != nil {
      log.Printf("Could not update indices due to error %s", err)
      return 0, err
    } else {
      return res["idx"].(int), nil
    }

  } else {
    err := indicesColl.Insert(map[string]interface{}{
      "_id": index,
      "idx": 1,
    })
    if err != nil {
      log.Printf("Could not create indices due to error %s", err)
      return 0, err
    } else {
      return 1, nil
    }
  }
}





func GetAllCategories(db *mgo.Database, conf config.Config) ([]models.Category, error) {
  results, err := GetCategories(db, conf, bson.M{})
  return results, err
}

func GetCategories(db *mgo.Database, conf config.Config, filter interface{}) ([]models.Category, error) {
  var results []models.Category
  catCollection := db.C("categories")
  err := catCollection.Find(filter).Limit(conf.CollectionLimit).All(&results)
  if err != nil {
    log.Printf("Could not retrieve collections from Mongo server due to error %s", err)
    return nil, err
  }
  return results, nil
}

func GetCategory(db *mgo.Database, conf config.Config, name string) (models.Category, error) {
  var results []models.Category
  catCollection := db.C("categories")
  err := catCollection.Find(bson.M{"category":name}).Limit(1).All(&results)
  if err != nil {
    log.Printf("Could not retrieve collections from Mongo server due to error %s", err)
    var res models.Category
    return res, err
  }
  if results == nil {
    log.Printf("No matching collections were found for the name %s", name)
    var res models.Category
    return res, errors.New("No collections found")
  }
  return results[0], nil
}

func GetTriggersForCategory(db *mgo.Database, conf config.Config, category models.Category, reqs []interface{}) ([]models.Trigger, error) {
  var results []models.Trigger
  trigCollection := db.C("triggers")
  catId := strconv.Itoa(category.Id)
  query := bson.M{
    "$and": append(reqs, bson.M{"category": catId}),
  }
  err := trigCollection.Find(query).Limit(conf.TriggerLimit).All(&results)
  if err != nil {
    log.Printf("Could not retrieve triggers from Mongo server due to error %s", err)
    return nil, err
  }
  return results, nil
}

func AddCategory(db *mgo.Database, conf config.Config, newcat models.Category) error {
  idx, err := GetNextIndex(db, "category")
  if err != nil {
    log.Printf("Aborting category insert...")
    return err
  }
  newcat.Id = idx
  catCollection := db.C("categories")
  cnt, err := catCollection.Find(bson.M{"name":newcat.GetName()}).Limit(1).Count()
  if err != nil {
    return err
  } else if cnt > 0 {
    log.Printf("Tried to insert category %s, which already exists.", newcat.GetName())
    return fmt.Errorf("That category already exists.")
  } else {
    err = catCollection.Insert(&newcat)
    if err != nil {
      log.Printf("Could not insert new category %s due to error %s", newcat.GetName(), err)
    }
    return err
  }
}

func AddTrigger(db *mgo.Database, conf config.Config, newtrig models.Trigger) error {
  catCollection := db.C("triggers")
  cnt, err := catCollection.Find(bson.M{"name":newtrig.Regex, "sound_data":newtrig.SoundData}).Limit(1).Count()
  if err != nil {
    return err
  } else if cnt > 0 {
    log.Printf("Tried to insert trigger for %s, which already exists.", newtrig.Regex)
    return fmt.Errorf("That category already exists.")
  } else {
    err = catCollection.Insert(&newtrig)
    if err != nil {
      log.Printf("Could not insert new triger for %s due to error %s", newtrig.Regex, err)
    }
    return err
  }
}
