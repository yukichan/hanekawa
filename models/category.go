package models

import (
  "fmt"
  "errors"
)

var raidTiers = [...]string {"Dungeon", "Trial", "Raid", "Extreme Trial", "Savage Raid"}
var jobs = [...]string {
  //Healer
  "WHM",
  "SCH",
  "AST",
  //Tank
  "PLD",
  "WAR",
  "DRK",
  //Magic DPS
  "BLM",
  "SMN",
  //Ranged DPS
  "BRD",
  "MCH",
  //Melee DPS
  "NIN",
  "MNK",
  "DRG",
  //Gathering
  "MIN",
  "BTN",
  "FSH",
  //Crafting
  "ALC",
  "ARM",
  "BSM",
  "CRP",
  "CUL",
  "GSM",
  "LTW",
  "WVR",
}

type CatTypes int
const (
  INSTANCE CatTypes = 1 + iota
  CLASS
  OTHER
)

type Category struct {
  Id int            `bson:"_id,omitempty"`
  Name string       `bson:"name"      json:"name"`
  Type CatTypes     `bson:"type"      json:"type"`
  RaidTier int      `bson:"tier"      json:"tier"`
  Expansion int     `bson:"expansion" json:"expansion"`
}


//Implementation of category interface for instance raids
func (c Category) GetCategoryXML() string {
  if c.Type == INSTANCE {
    return fmt.Sprintf("CategoryRestrict=\"True\" Category=\"%s\" ", c.Name)
  } else {
    return fmt.Sprintf("CategoryRestrict=\"False\" Category=\"%s\"", c.Name)
  }
}

func GetTiers() []string {
  return append(raidTiers[:], jobs[:]...)
}

func GetTierId(name string) (int, error) {
  for i, n := range GetTiers() {
    if n == name {
      return i, nil
    }
  }
  return -1, errors.New("No such name was found")
}

func (c Category) GetTierString() string {
  return GetTiers()[c.RaidTier]
}

func (c Category) GetName() string {
  return c.Name
}
