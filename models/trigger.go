package models

import (
  "fmt"
  "time"
  "log"
  "encoding/xml"
  "errors"
)

type Trigger struct {
  Regex string            `bson:"regex"      xml:"Regex"`
  SoundData string        `bson:"sound_data" xml:"SoundData"`
  SoundType int           `bson:"sound_type" xml:"SoundType"`
  Timer bool              `bson:"timer"      xml:"Timer"`
  TimerName string        `bson:"timer_name" xml:"TimerName"`
  Tabbed bool             `bson:"tabbed"     xml:"Tabbed"`
  Category int            `bson:"category"`
  Status int              `bson:"status"`
  Rating int              `bson:"rating"`
  Uploaded time.Time      `bson:"time"`
}

func btsCaps(in bool) string {
  if in {
    return "True"
  } else {
    return "False"
  }
}

func (t Trigger) ToXML(categoryXML string) string {
  var output string
  output =  "<Trigger "
  output += "Active=\"True\" "
  output += fmt.Sprintf("Regex=\"%s\" ", t.Regex)
  output += fmt.Sprintf("SoundData=\"%s\" ", t.SoundData)
  output += fmt.Sprintf("SoundType=\"%d\" ", t.SoundType)
  output += categoryXML + " "
  output += fmt.Sprintf("Timer=\"%s\" ", btsCaps(t.Timer))
  output += fmt.Sprintf("TimerName=\"%s\" ", t.TimerName)
  output += fmt.Sprintf("Tabbed=\"%s\" ", btsCaps(t.Tabbed))
  output += ">"

  return output
}

func FromXML(xmlStr string) ([]Trigger, error) {
  format, err := detectFormat(xmlStr)
  if err == nil {
    return nil, err
  }

  switch format {
  case 0:
    res := struct {
      CustomTriggers struct {
        Trigger []Trigger
      }
      SpellTimers []string
    }{}
    err = xml.Unmarshal([]byte(xmlStr), &res)
    if err != nil {
      log.Printf("Could not decode XML %s; failed with error %s", xmlStr, err)
      return nil, err
    } else {
      return res.CustomTriggers.Trigger, nil
    }
  case 1:
    var res []Trigger
    err = xml.Unmarshal([]byte(xmlStr), &res)
    if err != nil {
      log.Printf("Could not decode XML %s; failed with error %s", xmlStr, err)
      return nil, err
    } else {
      return res, nil
    }
  case 2:
    var res Trigger
    err = xml.Unmarshal([]byte(xmlStr), &res)
    if err != nil {
      log.Printf("Could not decode XML %s; failed with error %s", xmlStr, err)
      return nil, err
    } else {
      return []Trigger{res}, nil
    }
  case 3:
    log.Printf("Spell Timers are not currently supported.")
    return nil, errors.New("Spell timers are not currently supported")
  case 4:
    log.Printf("Spell Timers are not currently supported.")
    return nil, errors.New("Spell timers are not currently supported")
  }
  return nil, errors.New("Invalid format detected, now terminating...")
}

func detectFormat(xmlStr string) (int, error) {
  n := struct{
    XMLName xml.Name
  }{}
  err := xml.Unmarshal([]byte(xmlStr), &n)
  if err != nil {
    log.Printf("XML decode failed with error %s", err)
    return -1, err
  }

  switch n.XMLName.Local {
  case "Config":
    return 0, nil 
  case "CustomTriggers":
    return 1, nil
  case "Trigger":
    return 2, nil
  case "SpellTimers":
    return 3, nil
  case "Spell":
    return 4, nil
  }
  return -1, errors.New("Invalid root XML Tag found")
}

